package com.example.demo.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import com.example.demo.response.FileResponse;
import com.example.demo.service.StorageService;

import io.swagger.annotations.Api;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@CrossOrigin
@RestController
@Api(tags = "Files")
public class FileController {

	@Autowired
	 private StorageService storageService;
	 	 
	 public FileController(StorageService storageService) {
	        this.storageService = storageService;
	    }
	 
	 @GetMapping("/")
	    public Stream<Object> listAllFiles(Model model) {

	        model.addAttribute("files", storageService.loadAll().map(
	                path -> ServletUriComponentsBuilder.fromCurrentContextPath()
	                        .path("/download/")
	                        .path(path.getFileName().toString())
	                        .toUriString())
	                .collect(Collectors.toList()));
	        
	       Stream<Object> files = storageService.loadAll().map(
	                path -> ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/download/")
                    .path(path.getFileName().toString())
                    .toUriString());  
	       
	        return files;
	    }
	 
	 @GetMapping("/download/{filename:.+}")
	    @ResponseBody
	    public ResponseEntity<Resource> downloadFile(@PathVariable String filename) {

	        Resource resource = storageService.loadAsResource(filename);

	        return ResponseEntity.ok()
	                .header(HttpHeaders.CONTENT_DISPOSITION,
	                        "attachment; filename=\"" + resource.getFilename() + "\"")
	                .body(resource);
	    }

	 @PostMapping("/upload-file")
	    @ResponseBody
	    public FileResponse uploadFile(@RequestParam("file0") MultipartFile file) {
	    		    	
	        String name = storageService.store(file);

	        String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
	                .path("/download/")
	                .path(name)
	                .toUriString();

	        return new FileResponse(name, uri, file.getContentType(), file.getSize());
	    }
	 
	    @PostMapping("/upload-multiple-files")
	    @ResponseBody
	    public List<FileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
	        return Arrays.stream(files)
	                .map(file -> uploadFile(file))
	                .collect(Collectors.toList());
	    }
	 
}
